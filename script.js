/* init */
var ovr=false;          // game over status
var grid=new Array();   // the grid
var rows=11;            // rows
var cols=22;            // cols
var bper=15;            // percentage of bombs
var dbug=false;         // debug mode
var nln='\n';           // line break
var dng=true;           // allow diagonal calculations
var acls=true;          // clear fields around click mautomatically



/* add class to element */
function addClassToElementById(elmid,elmcls){
  var cls=getClassStringForElementById(elmid);
  if(cls.includes(elmcls)==false){
    var newcls=cls.concat(' '+elmcls);
    document.getElementById(elmid).className=newcls;
  }
}

/* remove class from element */
function removeClassFromElementById(elmid,elmcls){
  var cls=getClassStringForElementById(elmid);
  var newcls=cls.split(elmcls).join('');
  document.getElementById(elmid).className=newcls;
}

/* generate box id for click */
function getBoxIdForRowAndCol(r,c){
  return 'box-'+r+'-'+c;
}

/* get the string with class names for element */
function getClassStringForElementById(elmid){
  return document.getElementById(elmid).className;
}

/* do click on element by id */
function clickOnElementById(elmid){
  document.getElementById(elmid).click();
}

/* hide cover layer */
function hideCover(){
  addClassToElementById('cover1','cover1hide');
  addClassToElementById('cover2','cover2hide');
}

/* center game */
function centerWrapper(){
  var left=Math.floor((document.documentElement.clientWidth-document.getElementById('wrapper').offsetWidth)/2);
  document.getElementById('wrapper').style.marginLeft=left+'px';
  var top=Math.floor((document.documentElement.clientHeight-document.getElementById('wrapper').offsetHeight)/2);
  document.getElementById('wrapper').style.marginTop=top+'px';
}



/* generate dynamic game grid */
function generateGrid(){
  console.log('generate grid');
  for(r=0;r<rows;r++){
    grid[r]=new Array();
    for(c=0;c<cols;c++){
      // fill with zeros to start with
      grid[r][c]=0;
    }
  }
  console.log('generate bombs');
  // percentage depending on grid size
  var bombs=Math.floor((rows*cols)*(bper/100));
  for(b=0;b<bombs;b++){
    var rndrow=Math.floor(Math.random()*rows);
    var rndcol=Math.floor(Math.random()*cols);
    // the -1 as bomb marker
    grid[rndrow][rndcol]=-1;
  }
  console.log('calculate numbers');
  for(r=0;r<rows;r++){
    for(c=0;c<cols;c++){
      // calculate around a bomb
      if(grid[r][c]==-1){
        // vertical (check if not bomb)
        if(r-1>=0&&grid[r-1][c]>=0){grid[r-1][c]+=1;}
        if(r+1<rows&&grid[r+1][c]>=0){grid[r+1][c]+=1;}
        // horizontal (check if not bomb)
        if(c-1>=0&&grid[r][c-1]>=0){grid[r][c-1]+=1;}
        if(c+1<cols&&grid[r][c+1]>=0){grid[r][c+1]+=1;}
        // diagonal
        if(dng==true){
          if(r-1>=0&&c-1>=0&&grid[r-1][c-1]>=0){grid[r-1][c-1]+=1;}
          if(r-1>=0&&c+1<cols&&grid[r-1][c+1]>=0){grid[r-1][c+1]+=1;}
          if(r+1<rows&&c-1>=0&&grid[r+1][c-1]>=0){grid[r+1][c-1]+=1;}
          if(r+1<rows&&c+1<cols&&grid[r+1][c+1]>=0){grid[r+1][c+1]+=1;}
        }
      }
    }
  }
}

/* generate grid html */
function generateGridHtml(){
  console.log('generate grid html');
  var numstr='';
  var html='';
  for(r=0;r<rows;r++){
    for(c=0;c<cols;c++){
      // show numbers in debug mode
      if(dbug==true){numstr=grid[r][c];}
      html+='        <div class="box" id="box-'+r+'-'+c+'" attr-row="'+r+'" attr-col="'+c+'">'+numstr+'</div>'+nln;
    }
    html+='        <div class="clear"></div>'+nln;
  }
  return html;
}

/* set grid html */
function setGridHtml(html){
  console.log('set grid html');
  document.getElementById('boxes').innerHTML=html;
}

/* reveal all bombs */
function revealBombs(){
  // get possible elements for with bombs
  var elems=document.querySelectorAll('div.box:not(.clicked):not(.hnt)');
  elems.forEach((elm)=>{
    // get params
    var r=elm.getAttribute('attr-row');
    var c=elm.getAttribute('attr-col');
    // add css class for fields with bomb (-1)
    if(parseInt(grid[r][c])==-1){
      boxid=getBoxIdForRowAndCol(r,c);
      addClassToElementById(boxid,'bomb');
    }
  });
}

/* show random hint */
function showHint(){
  console.log('show hint');
  // prepare
  var boxid='';
  var cnt=0;
  // get possible elements for hint
  var elems=document.querySelectorAll('div.box:not(.clicked):not(.hnt)');
  // find a random one (try max elems.length)
  while(cnt<elems.length){
    // randome one
    var elm=elems[Math.floor(Math.random()*elems.length)];
    // get params
    var r=elm.getAttribute('attr-row');
    var c=elm.getAttribute('attr-col');
    // only '0' means it is ok for a hint
    var gnum=parseInt(grid[r][c]);
    if(gnum==0){
      boxid=getBoxIdForRowAndCol(r,c);
      break;
    }
    // reset
    boxid='';
    // count and continue
    cnt++;
  }
  // if there is a boxid set the class
  if(boxid.length>0){addClassToElementById(boxid,'hnt');}
}

/* check for click */
function checkForClick(r,c){
  console.log('check for click');
  // int them
  var r=parseInt(r);
  var c=parseInt(c);
  var gnum=parseInt(grid[r][c]);
  switch(gnum){
    // bomb (-1)
    case -1:
      endGame();
      var boxid=getBoxIdForRowAndCol(r,c);
      addClassToElementById(boxid,'end');
      break;
    // clickable (0)
    case 0:
      if(acls==true){clearAroundClick(r,c);}
      var boxid=getBoxIdForRowAndCol(r,c);
      addClassToElementById(boxid,'clicked');
      break;
    // around bomb (>0)
    default:
      var boxid=getBoxIdForRowAndCol(r,c);
      addClassToElementById(boxid,'clicked');
      document.getElementById(boxid).innerHTML=gnum;
      break;
  }
}

/* clear fields around click automatically */
function clearAroundClick(r,c){
  console.log('clear around click');
  // int them
  var r=parseInt(r);
  var c=parseInt(c);
  // get class from box to check
  var boxid=getBoxIdForRowAndCol(r,c);
  var bcls=getClassStringForElementById(boxid);
  // if not worked (clicked) with add class, show possible number and continue
  if(bcls.includes('clicked')==false){
    addClassToElementById(boxid,'clicked');
    if(parseInt(grid[r][c])>0){document.getElementById(boxid).innerHTML=parseInt(grid[r][c]);}
    // check next ones around a clickable (0) box and clear recursive
    if(r>0&&c>0&&parseInt(grid[r-1][c-1])>=0&&parseInt(grid[r][c])==0){clearAroundClick(r-1,c-1);}
    if(r>0&&parseInt(grid[r-1][c])>=0&&parseInt(grid[r][c])==0){clearAroundClick(r-1,c);}
    if(r>0&&c<cols-1&&parseInt(grid[r-1][c+1])>=0&&parseInt(grid[r][c])==0){clearAroundClick(r-1,c+1);}
    if(c>0&&parseInt(grid[r][c-1])>=0&&parseInt(grid[r][c])==0){clearAroundClick(r,c-1);}
    if(c<cols-1&&parseInt(grid[r][c+1])>=0&&parseInt(grid[r][c])==0){clearAroundClick(r,c+1);}
    if(r<rows-1&&c>0&&parseInt(grid[r+1][c-1])>=0&&parseInt(grid[r][c])==0){clearAroundClick(r+1,c-1);}
    if(r<rows-1&&parseInt(grid[r+1][c])>=0&&parseInt(grid[r][c])==0){clearAroundClick(r+1,c);}
    if(r<rows-1&&c<cols-1&&parseInt(grid[r+1][c+1])>=0&&parseInt(grid[r][c])==0){clearAroundClick(r+1,c+1);}
  }
  // nothing to do - return
  return;
}

/* did player win or not */
function checkForWin(){
  // get amount of un-clicked boxes
  var amt=document.querySelectorAll('div:not(.clicked)').length;
  // nothing to click anymore?
  if(amt==0){endGame();}
}



/* init game */
function init(){
  console.log('init');
  console.log('debug: '+dbug);
  grid=new Array();
  generateGrid();
  setGridHtml(generateGridHtml());
  if(dbug==true){revealBombs();}
  ovr=false;
  console.log('ready!');
}

/* end of game */
function endGame(){
  console.log('game ended');
  revealBombs();
  ovr=true;
}



/* listener for key */
document.addEventListener('keydown',(event)=>{
  switch(event.which){
    // h(elp)
    case 72:
      if(ovr==false){showHint();}
      break;    // n(ew)
    case 78:
      if(ovr==true){clickOnElementById('new');}
      break;
    // r(eveal)
    case 82:
      if(ovr==false){clickOnElementById('end');}
      break;
  }
},false);

/* listener for click */
document.addEventListener('click',(event)=>{
  // clicked on a button
  if(event.target.matches('.button')){
    var action=event.target.getAttribute('attr-action');
    console.log(action);
    // what to do?
    if(ovr==true&&action=='new'){init();}
    if(ovr==false&&action=='end'){endGame();}
  }
  // clicked on a box
	if(ovr==false&&event.target.matches('.box')){
    var row=parseInt(event.target.getAttribute('attr-row'));
    var col=parseInt(event.target.getAttribute('attr-col'));
    checkForClick(row,col);
    if(ovr==false){checkForWin();}
  }
},false);



/* wait until document is fully loaded and ready to start */
let stateCheck=setInterval(()=>{
  if(document.readyState=='complete'){
    clearInterval(stateCheck);
    init();
    centerWrapper();
    hideCover();
    setTimeout(()=>{addClassToElementById('wrapper','rotatein');},2500);
  }
},250);
